import { ref } from 'vue';
import type { FieldsOptionItem } from '@/types/FormControllerTypes';

export default function useGenerateModel(formData: FieldsOptionItem[]) {
  const formModel = ref<{
    [key: string]: string | number | Date | (string | number | Date)[] | undefined ;
  }>({});

  console.debug('---useGenerateMode: formData: ', formData)
  if(!formData.length) {
    return undefined;
  }

  formData.forEach((fieldsOptionItem) => {
    for (let groupItem of fieldsOptionItem['groupItems']) {
      formModel.value[groupItem.formItem.prop] =
        groupItem.type === 'ElCheckbox' ? [] : '';
    }
  });

  return formModel;
}
