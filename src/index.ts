import type { App } from 'vue';
import { FormController } from '@/components';
import type { FieldsOptionItem, FormOptions } from '@/types';

export default {
  install: (
    app: App,
    // options: { formOptions?: FormOptions; formFields?: FieldsOptionItem | {} } = {
    //   formOptions: {},
    //   formFields: {},
    // }
  ) => {
    app.component('FormController', FormController);
    // app.provide('formOptions', options.formOptions);
    // app.provide('formFields', options.formFields);
  },
};


export { FormController };