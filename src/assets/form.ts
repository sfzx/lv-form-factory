export const formData = [
  {
    row: {
      gutter: 20,
    },
    groupItems: [
      {
        type: 'ElInput',
        col: {
          span: 8,
        },
        formItem: {
          label: 'Name',
          prop: 'name',
        },
        input: {
          placeholder: 'Please enter your name',
        },
      },
      {
        type: 'ElInput',
        col: {
          span: 8,
        },
        formItem: {
          label: 'Last Name',
          prop: 'lastName',
        },
        input: {
          placeholder: 'Please enter your last name',
        },
      },
      {
        type: 'ElInput',
        col: {
          span: 8,
        },
        formItem: {
          label: 'Email',
          prop: 'email',
        },
        input: {
          type: 'email',
          placeholder: 'Please enter your Email',
        },
      },
    ],
  },
  {
    row: {
      gutter: 20,
    },
    groupItems: [
      {
        type: 'ElInput',
        col: {
          span: 12,
        },
        formItem: {
          label: 'Something',
          prop: 'some',
        },
        input: {
          placeholder: 'Enter something',
        },
      },
      {
        type: 'ElInput',
        col: {
          span: 12,
        },
        formItem: {
          label: 'Country',
          prop: 'country',
        },
        input: {
          placeholder: 'Enter country name',
        },
      },
    ],
  },
  {
    row: {
      gutter: 20,
    },
    groupItems: [
      {
        type: 'ElSelect',
        col: {
          span: 12,
        },
        formItem: {
          label: 'Age',
          prop: 'age',
        },
        input: {
          placeholder: 'Select age',
        },
        options: [
          {
            value: 1,
            label: 'One year',
          },
          {
            value: 2,
            label: 'Two year',
          },
          {
            value: 3,
            label: 'Three year',
          },
          {
            value: 4,
            label: 'Four year',
          },
        ],
      },
      {
        type: 'ElSelect',
        col: {
          span: 12,
        },
        formItem: {
          label: 'Animal',
          prop: 'animal',
        },
        input: {
          placeholder: 'Select animal',
        },
        options: [
          {
            value: 'Cat',
            label: 'cat',
          },
          {
            value: 'Dog',
            label: 'Dog',
          },
          {
            value: 'Rabbit',
            label: 'Rabbit',
          },
          {
            value: 'Mouse',
            label: 'Mouse',
          },
        ],
      },
    ],
  },
  {
    row: {
      gutter: 20,
    },
    groupItems: [
      {
        type: 'ElDatePicker',
        col: {},
        formItem: {
          label: 'Date range',
          prop: 'dataRange',
        },
        input: {
          rangeSeparator: 'To',
          startPlaceholder: 'Select start',
          endPlaceholder: 'Select end',
          type: 'daterange',
          valueFormat: 'YYYY-MM-DD',
        },
      },
    ],
  },
  {
    row: {
      gutter: 20,
    },
    groupItems: [
      {
        type: 'ElRadio',
        col: {
          span: 4,
        },
        formItem: {
          label: 'Radion',
          prop: 'radio',
        },
        input: [
          {
            label: 'radioSelect',
            text: 'Radio select',
          },
        ],
        radioGroup: {
          size: 'default',
        },
      },
      {
        type: 'ElCheckbox',
        col: {
          span: 4,
        },
        formItem: {
          label: 'Checkbox',
          prop: 'checkbox',
        },
        input: [
          {
            label: 'check this',
          },
          {
            label: 'secondCheck',
          },
        ],
        checkboxGroup: {
          max: 1,
        },
      },
    ],
  },
];
