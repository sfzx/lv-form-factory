import FormController from '@/components/FormController.vue'

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    FormController: typeof FormController
  }
}