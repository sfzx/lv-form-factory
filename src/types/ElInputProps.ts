import type { ElInput, InputAutoSize } from 'element-plus';
import type {
  Component,
  ComputedOptions,
  MethodOptions,
  StyleValue,
} from 'vue';

/**
 * https://element-plus.org/en-US/component/input.html#input-attributes
 */
export declare type ElInputProps = {
  [key: string]: any;
  type?: string;
  size?: '' | 'default' | 'small' | 'large';
  disabled?: boolean;
  label?: string;
  modelValue?: any;
  resize?: 'none' | 'both' | 'horizontal' | 'vertical';
  autosize?: InputAutoSize;
  autocomplete?: string;
  placeholder?: string;
  form?: string;
  readonly?: boolean;
  clearable?: boolean;
  showPassword?: boolean;
  showWordLimit?: boolean;
  suffixIcon?:
    | string
    | Component<any, any, any, ComputedOptions, MethodOptions>;
  prefixIcon?:
    | string
    | Component<any, any, any, ComputedOptions, MethodOptions>;
  tabindex?: string | number;
  validateEvent?: boolean;
  inputStyle?: StyleValue;
};

// export declare type ElInputProps =  Omit<InstanceType<typeof ElInput>, 'modelValue' | 'onUpdate:modelValue'> & {

// }
