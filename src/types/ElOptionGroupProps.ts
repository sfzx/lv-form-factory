import type { ElOptionGroup } from "element-plus";

/**
 * https://element-plus.org/en-US/component/select.html#option-group-attributes
 */
export declare type ElOptionGroupProps =  InstanceType<typeof ElOptionGroup>["$props"] & {

}