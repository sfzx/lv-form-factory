import type { ElRowProps } from './ElRowProps';
import type { ElColProps } from './ElColProps';
import type { ElFormItemProps } from './ElFormItemProps';
import type { ElCheckboxProps } from './ElCheckboxProps';
import type { ElRadioGroupProps } from './ElRadioGroupProps';
import type { ElRadioProps } from './ElRadioProps';
import type { ElCheckboxGroupProps } from './ElCheckboxGroupProps';
import type { ElOptionProps } from './ElOptionProps';
import type { ElSelectProps } from './ElSelectProps';
import type { ElInputProps } from './ElInputProps';
import type { ElInputNumberProps } from './ElInputNumberProps';
import type { ElDatePickerProps } from './ElDatePickerProps';
import type { Component } from 'vue';

export enum GroupItemEnum {
  ElInput = 'ElInput',
  ElInputNumber = 'ElInputNumber',
  ElSelect = 'ElSelect',
  ElDatePicker = 'ElDatePicker',
  ElRadio = 'ElRadio',
  ElCheckbox = 'ElCheckbox',
  ElCustomComponent = 'ElCustomComponent',
}

interface RadioInput {
  type: GroupItemEnum.ElRadio;
  input: ElRadioProps[];
  radioGroup: ElRadioGroupProps;
}

interface CheckboxInput {
  type: GroupItemEnum.ElCheckbox;
  input: ElCheckboxProps[];
  checkboxGroup: ElCheckboxGroupProps;
}

interface SelectInput {
  type: GroupItemEnum.ElSelect;
  input: ElSelectProps;
  options: ElOptionProps[];
}

interface Input {
  type: GroupItemEnum.ElInput;
  input: ElInputProps;
}

interface InputNumber {
  type: GroupItemEnum.ElInputNumber;
  input: ElInputNumberProps;
}

interface DatePickerInput {
  type: GroupItemEnum.ElDatePicker;
  input: ElDatePickerProps;
}

interface CustomComponent {
  type: GroupItemEnum.ElCustomComponent;
  customComponent: Component;
}

export type GroupItem = {
  col?: ElColProps | null;
  formItem: ElFormItemProps;
} & (
  | DatePickerInput
  | InputNumber
  | Input
  | SelectInput
  | CheckboxInput
  | RadioInput
  | CustomComponent
);

export type FieldsOptionItem = {
  row?: ElRowProps | null;
  groupItems: GroupItem[];
};

/**
 * https://element-plus.org/en-US/component/form.html#form-api
 */
export interface FormOptions {
  [key: string]: any;
  inline?: boolean;
  'label-position'?: 'left' | 'right' | 'top';
  'label-width'?: string | number;
  'label-suffix'?: string;
  'hide-required-asterisk'?: boolean;
  'show-message'?: boolean;
  'inline-message'?: boolean;
  'status-icon'?: boolean;
  'validate-on-rule-change'?: boolean;
  size?: 'large' | 'default' | 'small';
  disabled?: boolean;
  'scroll-to-error'?: boolean;
}
