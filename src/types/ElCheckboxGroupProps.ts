import type { ElCheckboxGroup } from "element-plus";

/**
 * https://element-plus.org/en-US/component/checkbox.html#checkbox-group-attributes
 */
export type ElCheckboxGroupProps = {
  [key: string]: any;
  disabled?: boolean;
  fill?: string;
  textColor?: string;
  tag?: string;
  max?: number;
  min?: number;
};

// export declare type ElCheckboxGroupProps = InstanceType<typeof ElCheckboxGroup> & {

// }