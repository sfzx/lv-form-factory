import type { ElFormItem } from "element-plus";

// TODO: implement form item types
/**
 * https://element-plus.org/en-US/component/form.html#form-item-attributes
 */
export declare type ElFormItemProps = {
  [key: string]: any;
  readonly prop: string;
  readonly label: string;
};

// export declare type ElFormItemProps = InstanceType<typeof ElFormItem> & {
//   readonly prop: string;
// };
