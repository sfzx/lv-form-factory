import type { ElCheckbox } from "element-plus";

/**
 * https://element-plus.org/en-US/component/checkbox.html#checkbox-group-attributes
 */
export declare type ElCheckboxProps = {
  [key: string]: any;
  disabled?: boolean;
  name?: string;
  modelValue?: string | number | boolean;
  border?: boolean;
  id?: string;
  indeterminate?: boolean;
  checked?: boolean;
  trueLabel?: string | number;
  falseLabel?: string | number;
  controls?: string;
};

// export declare type ElCheckboxProps = InstanceType<typeof ElCheckbox>["$props"] & {

// }