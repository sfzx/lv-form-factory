import type { ElDatePicker, Options } from "element-plus";
import type { IDatePickerType } from "element-plus/es/components/date-picker/src/date-picker.type";
import type { Component, ComputedOptions, MethodOptions } from "vue";

/**
 * https://element-plus.org/en-US/component/date-picker.html#attributes
 */
export declare type ElDatePickerProps = {
  [key: string]: any
  type?: IDatePickerType;
  disabled?: boolean;
  name?: string;
  modelValue?: any;
  editable?: boolean;
  popperClass?: string;
  popperOptions?: Partial<Options>;
  placeholder?: string;
  readonly?: boolean;
  clearable?: boolean;
  prefixIcon?: string | Component<any, any, any, ComputedOptions, MethodOptions>;
  validateEvent?: boolean;
  clearIcon?: string | Component<any, any, any, ComputedOptions, MethodOptions>;
  rangeSeparator?: string;
  isRange?: boolean;
  shortcuts?: unknown[];
  arrowControl?: boolean;
  unlinkPanels?: boolean;
}

// export declare type ElDatePickerProps =  Omit<InstanceType<typeof ElDatePicker>, 'modelValue'>['$props'] & {
    
// }