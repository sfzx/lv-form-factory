import type { ElOption } from "element-plus";

/**
 * https://element-plus.org/en-US/component/select.html#option-attributes
 */
export declare type ElOptionProps = {
  [key: string]: any;
  value: any;
  label: string;
};

// export declare type ElOptionProps =  InstanceType<typeof ElOption> & {

// }