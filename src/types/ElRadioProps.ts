import type { ElRadio } from 'element-plus';

/**
 * https://element-plus.org/en-US/component/radio.html#radio-attributes
 */
export declare type ElRadioProps = {
  /** The value of Radio */
  label?: string | number | boolean;
  /** The label of Radio */
  text?: string;
  size?: '' | 'default' | 'small' | 'large';
  disabled?: boolean;
  name?: string;
  modelValue?: string | number | boolean;
  border?: boolean;
};

// export declare type ElRadioProps =  InstanceType<typeof ElRadio> & {
//     /** The value of Radio */
//     label?: BuildPropType<(BooleanConstructor | StringConstructor | NumberConstructor)[], unknown, unknown>;
//     /** The label of Radio */
//     text?: string;
// }
