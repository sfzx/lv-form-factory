/**
 * https://element-plus.org/en-US/component/input.html#input-attributes
 */
export declare type ElInputNumberProps = {
  [key: string]: any;
  size?: '' | 'default' | 'small' | 'large';
  disabled?: boolean;
  modelValue?: number;
  max?: number;
  controls?: boolean;
  min?: number;
  step?: number;
  stepStrictly?: boolean;
  controlsPosition?: '' | 'right';
  precision?: number;
};

// export declare type ElInputNumberProps =  InstanceType<typeof ElInputNumber>["$props"] & {

// }