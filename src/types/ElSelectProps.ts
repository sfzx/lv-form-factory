import type { ElSelect } from "element-plus";

/**
 * https://element-plus.org/en-US/component/select.html#select-attributes
 */
export declare type ElSelectProps = {
  [key: string]: any;
}; 

// export declare type ElSelectProps =  InstanceType<typeof ElSelect> & {

// }