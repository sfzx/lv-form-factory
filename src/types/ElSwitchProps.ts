import type { ElSwitch } from "element-plus";

/**
 * https://element-plus.org/en-US/component/switch.html#attributes
 */
export declare type ElSwitchProps =  InstanceType<typeof ElSwitch>["$props"] & {

}