import type { ElRadioGroup } from "element-plus";

/**
 * https://element-plus.org/en-US/component/radio.html#radio-group-attributes
 */
export declare type ElRadioGroupProps = {
  [key: string]: any;
  size?: '' | 'default' | 'small' | 'large'
  disabled?: boolean;
  fill?: string;
  textColor?: string;
};

// export declare type ElRadioGroupProps =  InstanceType<typeof ElRadioGroup>["$props"] & {

// }