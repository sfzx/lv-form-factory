import type { ElRow } from 'element-plus';

/**
 * https://element-plus.org/en-US/component/layout.html#row-attributes
 */
export type ElRowProps = {
  [key: string]: any;
  justify?:
    | 'center'
    | 'space-around'
    | 'space-between'
    | 'space-evenly'
    | 'end'
    | 'start';
  tag?: string;
  align?: 'top' | 'bottom' | 'middle';
  gutter?: number;
};

// export type ElRowProps = InstanceType<typeof ElRow> & {

// }
