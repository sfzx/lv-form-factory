/**
 * https://element-plus.org/en-US/component/layout.html#col-attributes
 */

export type ElColGrid = {
  push?: number;
  offset?: number;
  span?: number;
  pull?: number;
};

export declare type ElColProps = ElColGrid & {
  [key: string]: any;
  tag?: string;
  xs?: number | ElColGrid;
  sm?: number | ElColGrid;
  md?: number | ElColGrid;
  lg?: number | ElColGrid;
  xl?: number | ElColGrid;
};

// export declare type ElColProps = InstanceType<typeof ElCol>['$props'] & {
//   [key: string]: any;
// };
