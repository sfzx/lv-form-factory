# Package was created fot learning purpose !

- Vue 3 Composition API

## Quick start

Install Package 
```bash
npm install lv-form-factory    
```

Then import the full bundle
```
import { createApp } from 'vue'
import FormController from "lv-form-factory";
import 'lv-form-factory/dist/style.css';

createApp(...).use(FormController)
```

## API Reference 
- `formOptions` : 
[parameters](https://element-plus.org/en-US/component/form.html#form-api) 


- `formFields` :

| Attribute     | Description   | Types             |
| ------------- | ------------- |:-----------------:| 
| row           | ElRowProps    | ElRowProps | null |    
| groupItems    | GroupItem     | GroupItem []      |   

- `ElRowProps` :
[parameters](https://element-plus.org/en-US/component/layout.html#row-attributes)

- `GroupItem`


## Component usage example 
```
<form-controller :formOptions="{}" :formFields="[]"></form-controller>
```

## Data sample to use
[groupItems](https://gitlab.com/sfzx/lv-form-factory/-/blob/master/src/assets/form.ts)
 